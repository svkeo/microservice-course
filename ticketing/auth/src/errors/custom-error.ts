export abstract class CustomError extends Error {
  abstract statusCode: number;

  constructor(message: string) {
    // For logging purposes
    super(message);

    // Only because we are extending a built in class
    // https://stackoverflow.com/questions/41102060/typescript-extending-error-class
    Object.setPrototypeOf(this, new.target.prototype);
  }

  abstract serializeErrors(): { message: string; field?: string; }[];
}