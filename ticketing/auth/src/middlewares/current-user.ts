import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';

interface UserPayload {
  id: string;
  email: string;
}

declare global {
  namespace Express {
    interface Request {
      currentUser?: UserPayload;
    }
  }
}

export const currentUser = (req: Request, res: Response, next: NextFunction) => {
  // Equivalent to (!req.session || !req.session.jwt)
  if (!req.session?.jwt) {
    return next();
  }

  try {
    // Checking in index.ts for missing config on startup
    const payload = jwt.verify(req.session.jwt, process.env.JWT_KEY!) as UserPayload;
    req.currentUser = payload;
  } finally {
    next();
  }
};

// try {
//   // Checking in index.ts for missing config on startup
//   const payload = jwt.verify(req.session.jwt, process.env.JWT_KEY!) as UserPayload;
//   req.currentUser = payload;
// } catch (err) { }